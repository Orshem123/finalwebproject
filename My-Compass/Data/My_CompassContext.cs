﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using My_Compass.Models;

namespace My_Compass.Data
{
    public class My_CompassContext : DbContext
    {
        public My_CompassContext (DbContextOptions<My_CompassContext> options)
            : base(options)
        {
        }

        public DbSet<My_Compass.Models.TripEventModel> TripEventModel { get; set; }
    }
}
