﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace My_Compass.Models
{
    public class TripEventModel
    {
       
        public string Id { get;  set; }
        public string title { get; set; }
        public string category { get; set; }
        public string datetime { get; set; }
        public int max_attendies { get; set; }

        public int duration { get; set; }
        public string location { get; set; }
        public string notes { get; set; }

    }
}
