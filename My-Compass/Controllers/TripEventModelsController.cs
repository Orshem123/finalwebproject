﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using My_Compass.Data;
using My_Compass.Models;

namespace My_Compass.Controllers
{
    public class TripEventModelsController : Controller
    {
        private readonly My_CompassContext _context;

        public TripEventModelsController(My_CompassContext context)
        {
            _context = context;
        }

        // GET: TripEventModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.TripEventModel.ToListAsync());
        }

        // GET: TripEventModels/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripEventModel = await _context.TripEventModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tripEventModel == null)
            {
                return NotFound();
            }

            return View(tripEventModel);
        }

        // GET: TripEventModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TripEventModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,title,category,datetime,max_attendies,duration,location,notes")] TripEventModel tripEventModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tripEventModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tripEventModel);
        }

        // GET: TripEventModels/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripEventModel = await _context.TripEventModel.FindAsync(id);
            if (tripEventModel == null)
            {
                return NotFound();
            }
            return View(tripEventModel);
        }

        // POST: TripEventModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,title,category,datetime,max_attendies,duration,location,notes")] TripEventModel tripEventModel)
        {
            if (id != tripEventModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tripEventModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TripEventModelExists(tripEventModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tripEventModel);
        }

        // GET: TripEventModels/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripEventModel = await _context.TripEventModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tripEventModel == null)
            {
                return NotFound();
            }

            return View(tripEventModel);
        }

        // POST: TripEventModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var tripEventModel = await _context.TripEventModel.FindAsync(id);
            _context.TripEventModel.Remove(tripEventModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TripEventModelExists(string id)
        {
            return _context.TripEventModel.Any(e => e.Id == id);
        }
    }
}
